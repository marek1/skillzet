import { Action } from "redux";
import { ThunkAction } from "redux-thunk";
import { AppState } from "../store";
import { selectSkillTag, setUsername, setUserProfession } from "../store/user/actions";
import { AddedSkillInterface } from "../interfaces/AddedSkillInterface";

export const thunkGetUserProfile = (): ThunkAction<void, AppState, null, Action<any>> => async dispatch => {
	const asyncResp = await retrieveProfile();


	if (asyncResp) {

		let _localStorage = JSON.parse(asyncResp);

		if (_localStorage.userName !== 'undefined') {
			dispatch(
				setUsername(_localStorage.userName)
			)
		}

		if (_localStorage.userProfession !== 'undefined') {
			dispatch(
				setUserProfession(_localStorage.userProfession)
			)
		}

		if (_localStorage.selectedSkillTags !== 'undefined') {
			if (_localStorage.selectedSkillTags.map !== 'undefined') {
				_localStorage.selectedSkillTags.map(
					(selectedSkillTag: AddedSkillInterface) => dispatch(selectSkillTag(selectedSkillTag)))
			}
		}

	}

};



function retrieveProfile() {
	// let newAddedSkill: AddedSkillInterface = {
	// 	name: 'Soziale Medien / Social Media',
	// 	added: new Date(),
	// 	skillLevel: SkillLevels.Beginner
	// };

	let userProfile = window.localStorage.getItem('skillzet_UserProfile');

	return Promise.resolve(userProfile);
}
