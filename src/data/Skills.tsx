import { SkillInterface } from "../interfaces/SkillInterface";
import { SkillTypes } from "../enums/SkillTypes";

export const Skills: SkillInterface[] = [
    {
        name: 'Selbstvertrauen',
        type: SkillTypes.Softskills,
        subtype: 'Persönliche Kompetenz'
    },{
        name: 'Selbstdisziplin',
        type: SkillTypes.Softskills,
        subtype: 'Persönliche Kompetenz'
    },{
        name: 'Selbstreflexion',
        type: SkillTypes.Softskills,
        subtype: 'Persönliche Kompetenz'
    },{
        name: 'Engagement',
        type: SkillTypes.Softskills,
        subtype: 'Persönliche Kompetenz'
    },{
        name: 'Motivation',
        type: SkillTypes.Softskills,
        subtype: 'Persönliche Kompetenz'
    },{
        name: 'Neugier',
        type: SkillTypes.Softskills,
        subtype: 'Persönliche Kompetenz'
    },{
        name: 'Belastbarkeit',
        type: SkillTypes.Softskills,
        subtype: 'Persönliche Kompetenz'
    },{
        name: 'Eigenverantwortung',
        type: SkillTypes.Softskills,
        subtype: 'Persönliche Kompetenz'
    },{
        name: 'Teamfähigkeit',
        type: SkillTypes.Softskills,
        subtype: 'Soziale Kompetenz'
    },{
        name: 'Einfühlvermögen / Empathie',
        type: SkillTypes.Softskills,
        subtype: 'Soziale Kompetenz'
    },{
        name: 'Menschenkenntnis',
        type: SkillTypes.Softskills,
        subtype: 'Soziale Kompetenz'
    },{
        name: 'Kommunikationsfähigkeit',
        type: SkillTypes.Softskills,
        subtype: 'Soziale Kompetenz'
    },{
        name: 'Integrationsbereitschaft',
        type: SkillTypes.Softskills,
        subtype: 'Soziale Kompetenz'
    },{
        name: 'Kritikfähigkeit',
        type: SkillTypes.Softskills,
        subtype: 'Soziale Kompetenz'
    },{
        name: 'Umgangsstil',
        type: SkillTypes.Softskills,
        subtype: 'Soziale Kompetenz'
    },{
        name: 'Präsentationstechniken',
        type: SkillTypes.Softskills,
        subtype: 'Methodische Kompetenz'
    },{
        name: 'Umgang mit Neuen Medien',
        type: SkillTypes.Softskills,
        subtype: 'Methodische Kompetenz'
    },{
        name: 'Strukturierte und zielorientierte Arbeitsweise',
        type: SkillTypes.Softskills,
        subtype: 'Methodische Kompetenz'
    },{
        name: 'Analytische Fähigkeiten',
        type: SkillTypes.Softskills,
        subtype: 'Methodische Kompetenz'
    },{
        name: 'Problemlösungskompetenz / problem solving',
        type: SkillTypes.Softskills,
        subtype: 'Methodische Kompetenz'
    },{
        name: 'Stressresistenz / stress resistance',
        type: SkillTypes.Softskills,
        subtype: 'Methodische Kompetenz'
    },{
        name: 'Organisationstalent',
        type: SkillTypes.Softskills,
        subtype: 'Methodische Kompetenz'
    },{
        name: 'Führung / Leadership',
        type: SkillTypes.Softskills,
        subtype: 'Führungsqualitäten'
    },{
        name: 'Kommuikation / Communication',
        type: SkillTypes.Softskills,
        subtype: 'Führungsqualitäten'
    },{
        name: 'Verhandlungssicherheit / Negotiating',
        type: SkillTypes.Softskills,
        subtype: 'Führungsqualitäten'
    },{
        name: 'Kritisches Denken  / Critical Thinking',
        type: SkillTypes.Softskills,
        subtype: 'Führungsqualitäten'
    },{
        name: 'Humor',
        type: SkillTypes.Softskills,
        subtype: 'Führungsqualitäten'
    }, {
        name: 'Softwareentwicklung',
        type: SkillTypes.Hardskills,
        subtype: 'Anwendungsentwicklung'
    },{
        name: 'Mobile App Entwicklung',
        type: SkillTypes.Hardskills,
        subtype: 'Anwendungsentwicklung'
    },{
        name: 'Frontend Webentwicklung',
        type: SkillTypes.Hardskills,
        subtype: 'Anwendungsentwicklung'
    },{
        name: 'Backend Webentwicklung',
        type: SkillTypes.Hardskills,
        subtype: 'Anwendungsentwicklung'
    },{
        name: 'IOT Entwicklung',
        type: SkillTypes.Hardskills,
        subtype: 'Anwendungsentwicklung'
    },{
        name: 'Embedded Entwicklung',
        type: SkillTypes.Hardskills,
        subtype: 'Anwendungsentwicklung'
    },{
        name: 'Web Design',
        type: SkillTypes.Hardskills,
        subtype: 'Anwendungsentwicklung'
    },{
        name: 'Game Design',
        type: SkillTypes.Hardskills,
        subtype: 'Anwendungsentwicklung'
    },{
        name: 'Spieleentwicklung / Game Engineering',
        type: SkillTypes.Hardskills,
        subtype: 'Anwendungsentwicklung'
    },{
        name: 'Programmierung',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'HTML',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'CSS',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Javascript',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Typescript',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'CoffeeScript',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'PHP',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Ruby',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Ruby on Rails',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'C++',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'C#',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Java',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'GoLang',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Kotlin',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Swift',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Objective C',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Python',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'VB.NET',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'R',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Perl',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Go',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Scala',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Rust',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Shell / Powershell',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Elixir',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Fortran',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Matlab',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Julia',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'OCaml',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Haskell',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Assembly',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Arduino',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Dart',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Lua',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Erlang',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'F#',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Emacs Lisp',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Puppet',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'D',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'TeX',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'ColdFusion',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'QML',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Tcl',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Processing',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Mathematica',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Pascal',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Elm',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Apex',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Saltstack',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Vala',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Crystal',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Roff',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Smalltalk',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'TeX',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'ActionScript',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Cuda',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Prolog',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Scheme',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'VHDL',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Racket',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Protocol Buffer',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Verilog',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Freemarker',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Liquid markup language',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'RobotFramework',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'NSIS',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Yacc',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Coq',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Haxe',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'PostScript',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Gherkin',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'BitBake',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'PureScript',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Modelica',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Chapel',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Nim',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'MaxScript',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Logos',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'GAP',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'API Blueprint',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Web Assembly',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'SMT',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'RAML',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'M4',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Command Language',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Gosu',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'Renpy',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'GDScript',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'KiCad',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: '1C Enterprise',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'SQF',
        type: SkillTypes.Hardskills,
        subtype: 'Programmierung'
    },{
        name: 'JIRA',
        type: SkillTypes.Hardskills,
        subtype: 'Entwicklungswerkzeuge'
    },{
        name: 'Confluence',
        type: SkillTypes.Hardskills,
        subtype: 'Entwicklungswerkzeuge'
    },{
        name: 'Versionierung',
        type: SkillTypes.Hardskills,
        subtype: 'Entwicklungswerkzeuge'
    },{
        name: 'Trello',
        type: SkillTypes.Hardskills,
        subtype: 'Entwicklungswerkzeuge'
    },{
        name: 'Azure DevOps',
        type: SkillTypes.Hardskills,
        subtype: 'Entwicklungswerkzeuge'
    },{
        name: 'Asana',
        type: SkillTypes.Hardskills,
        subtype: 'Entwicklungswerkzeuge'
    },{
        name: 'GIT',
        type: SkillTypes.Hardskills,
        subtype: 'Entwicklungswerkzeuge'
    },{
        name: 'SVN / Subversion',
        type: SkillTypes.Hardskills,
        subtype: 'Entwicklungswerkzeuge'
    },{
        name: 'Mercurial',
        type: SkillTypes.Hardskills,
        subtype: 'Entwicklungswerkzeuge'
    },{
        name: 'Testing / QA',
        type: SkillTypes.Hardskills,
        subtype: 'Qualitätssicherung'
    },{
        name: 'Technisches Schreiben',
        type: SkillTypes.Hardskills,
        subtype: 'Technisches Schreiben'
    },{
        name: 'Technischer Support',
        type: SkillTypes.Hardskills,
        subtype: 'Technischer Support'
    },{
        name: 'Datenvisualisierung / Data visualization',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Datenanalyse / Data analytics',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Datenintuition / Data Intuition',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Statistik / Statistical analytics',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Data Wrangling',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Machine Learning',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Datenkategorisierung / Data Categorization',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Dateninterpretation / Data Interpretation',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Datenmodelle / Data modelling',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Data warehousing',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Data staging',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Large scale computing',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Data optimization',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Spreadsheeting/ Spreadsheet analytics',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Data cleaning',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Mathematik',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Predictive Analytics',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Künstliche Intelligenz (KI) / Artificial Intelligence (AI)',
        type: SkillTypes.Hardskills,
        subtype: 'Data Analytics / Business Intelligence'
    },{
        name: 'Datenbankenadministration',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'Relationale Datenbanken',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'SQL',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'NOSQL / Nicht relationale Datenbanken',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'Oracle DB',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'MariaDB',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'PostgreSQL',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'MSSQL',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'MySQL',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'DB2',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'Hadoop',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'MongoDB',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'Redis',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'Cassandra',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'CouchDB',
        type: SkillTypes.Hardskills,
        subtype: 'Database Administration'
    },{
        name: 'Cloud Computing',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Windows Server',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Unix Server',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'ngix',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Continuous Integration (CI)',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Continuous Develivery (CD)',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Continuous Deployment',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Continuous Monitoring',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Continuous Testing',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Containermanagement',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Serverless',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Systemtest / System Testing',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Configuration Management',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'System Design',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Systemanalyse / System Analysis',
        type: SkillTypes.Hardskills,
        subtype: 'Admin / Devops'
    },{
        name: 'Netzwerke',
        type: SkillTypes.Hardskills,
        subtype: 'Netzwerk Administration'
    },{
        name: 'Router / Routing',
        type: SkillTypes.Hardskills,
        subtype: 'Netzwerk Administration'
    },{
        name: 'Switch / Switching',
        type: SkillTypes.Hardskills,
        subtype: 'Netzwerk Administration'
    },{
        name: 'Netzwerksicherheit / Network Security',
        type: SkillTypes.Hardskills,
        subtype: 'Netzwerk Administration'
    },{
        name: 'Netzwerkdesign / Network Design',
        type: SkillTypes.Hardskills,
        subtype: 'Netzwerk Administration'
    },{
        name: 'Netzwerkzugang / Network Access',
        type: SkillTypes.Hardskills,
        subtype: 'Netzwerk Administration'
    },{
        name: 'Datenzentrum / Data Center',
        type: SkillTypes.Hardskills,
        subtype: 'Netzwerk Administration'
    },{
        name: 'Planung / Scheduling',
        type: SkillTypes.Hardskills,
        subtype: 'Projektmanagement'
    },{
        name: 'Risikomanagment / Risk management',
        type: SkillTypes.Hardskills,
        subtype: 'Projektmanagement'
    },{
        name: 'Kostenmangement / Cost management',
        type: SkillTypes.Hardskills,
        subtype: 'Projektmanagement'
    },{
        name: 'Aufgabenmanagement / Task management',
        type: SkillTypes.Hardskills,
        subtype: 'Projektmanagement'
    },{
        name: 'Qualitätsmanagement / Quality management',
        type: SkillTypes.Hardskills,
        subtype: 'Projektmanagement'
    },{
        name: 'Design',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'User Interaction (UI))',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'User Exeperience (UX)',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Konzeption',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'User research',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Interaction design',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Service design',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Information design',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Visual design',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Branding',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Usability Engineering',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Information architecture',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Wireframing',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Prototyping',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'Story telling',
        type: SkillTypes.Hardskills,
        subtype: 'Design / UI / UX'
    },{
        name: 'SEO',
        type: SkillTypes.Hardskills,
        subtype: 'Online Marketing'
    },{
        name: 'SEM',
        type: SkillTypes.Hardskills,
        subtype: 'Online Marketing'
    },{
        name: 'Content Marketing',
        type: SkillTypes.Hardskills,
        subtype: 'Online Marketing'
    },{
        name: 'Soziale Medien / Social Media',
        type: SkillTypes.Hardskills,
        subtype: 'Online Marketing'
    },{
        name: 'Mobile Marketing',
        type: SkillTypes.Hardskills,
        subtype: 'Online Marketing'
    }
];
