import { NewsInterface } from "../interfaces/NewsInterface";

export const NewsArray: NewsInterface[] = [
	{
		header: 'Die 3-meistgesuchten IT-Skills',
		body: 'Laut diesem Artikel sind IT-Skills nach ca. 6 Jahren veraltet. Für ITler bedeutet das, daß sie anpassungsfähig sein müssen. ' +
			'Neben Anpassungsfähigkeit ist auch der Wille zur Weiterbildung gefragt. Aber alles in Allem seien folgende Softskills immer ' +
			'wichtiger : kritisches Denken, Kreativität und die Fähigkeit, Probleme zu lösen.',
		imageLink: 'https://assets.t3n.sc/news/wp-content/uploads/2019/07/digitale-skills-teaser.jpg?auto=format&fit=crop&h=348&ixlib=php-2.3.0&w=620',
		link: 'https://t3n.de/news/drei-meistgesuchten-tech-skills-1190320/',
	},
	{
		header: 'Softskills sind immer gefragter',
		body: 'Softskills, also weiche Fähigkeiten, werden immer wichtiger im Arbeitsleben. Dazu gehören ... ',
		imageLink: 'https://www.bund-verlag.de/.imaging/mte/bund-de-theme/news-teaser-detail-image/dam/Wordpress/Dollarphotoclub_49975855_160503.jpg/jcr:content/Dollarphotoclub_49975855_160503.jpg.JPG',
		link: 'https://www.bund-verlag.de/aktuelles~Diese-Soft-Skills-sollten-Betriebsraete-mitbringen~'
	},
	{
		header: 'Data Scientists gefragt wie nie',
		body: 'Laut  Merten Slominsky wird die Nachfrage nach Data Scientists bis 2022 weiter steigen. Er sieht die Fähigkeit, ' +
			'Daten analysieren zu können als einen extrem zukunftsträchtigen an.',
		imageLink: '',
		link: 'https://www.bigdata-insider.de/data-scientists-gefragt-gejagt-a-852195/'
	}
];