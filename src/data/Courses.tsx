import { CourseInterface } from "../interfaces/CourseInterface";

export const Courses: CourseInterface[] = [
	{
		id: 0,
		name: 'Softwareentwicklung',
		description: 'Lorem ipsum',
		image: 'http://www.brandenbourger.de/images/dienstleistungen/softwareEngineering_mashup.jpg'
	},
	{
		id: 1,
		name: 'App Entwicklung',
		description: 'Lorem ipsum',
		image: 'http://www.qbeans.de/assets/img/leistungen/app/app-entwicklung-berlin.png'
	},
	{
		id: 2,
		name: 'Frontend Webentwicklung',
		description: 'Lorem ipsum',
		image: 'https://s3-eu-west-1.amazonaws.com/static.campusjaeger.de/site/photo/92/frontend-entwickler.png'
	},
	{
		id: 3,
		name: 'Backend Webentwicklung',
		description: 'Lorem ipsum',
		image: 'https://st4.depositphotos.com/1092019/19763/i/1600/depositphotos_197632794-stock-photo-now-hiring-back-end-developer.jpg'
	},
];
