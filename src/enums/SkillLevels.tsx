/*
Beginner = 'Geringes Kompetenzlevel',
Average = 'Durchschnittliches hohes Kompetenzlevel',
Skilled = 'Gutes Kompetenzlevel',
Specialist = 'Hohes Kompetenzlevel',
Expert = 'Außerordentlich hohes Kompetenzlevel'
*/

export enum SkillLevels {
	Beginner = 1,
	Average = 2,
	Skilled = 3,
	Specialist = 4,
	Expert = 5
};
