import { SkillTypes } from "../enums/SkillTypes";

export interface SkillInterface {
    name: string;
    type: SkillTypes;
    subtype: string|null;
}
