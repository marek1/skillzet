export interface CourseInterface {
	id: number;
	name: string;
	description: string;
	image: string | null;
}