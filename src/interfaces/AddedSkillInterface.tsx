import { SkillLevels } from "../enums/SkillLevels";

export interface AddedSkillInterface {
	name: string;
	added: Date;
	skillLevel: SkillLevels;
}
