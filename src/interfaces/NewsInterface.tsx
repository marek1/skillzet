export interface NewsInterface {
	header: string;
	body: string;
	imageLink: string|null;
	link: string|null;
}
