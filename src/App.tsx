import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Profile from "./components/Profile";
import News from "./components/News";
import ManageSkills from "./components/ManageSkills";
import "./App.css";

export default class App extends React.Component<{}> {

	state = {
		value: window.location.pathname
	};

	setPathname = (e: React.MouseEvent<HTMLAnchorElement>) => {
		this.setState({value: e.currentTarget.getAttribute('href')});
	};

	render() {

		return (

			<Router>
				<header>
					<Link to={'/'} className={this.state.value === '/' ? 'button button-underline' : 'button' } onClick={this.setPathname}>Home</Link>
					<Link to={'/profile'} className={this.state.value  === '/profile' ? 'button button-underline' : 'button'} onClick={this.setPathname}>Profile</Link>
				</header>

				<Switch>
					<Route exact path='/' component={News} />
					<Route path='/profile' component={Profile} />
					<Route path='/manage-skills' component={ManageSkills} />
				</Switch>
			</Router>

		);
	};

};

