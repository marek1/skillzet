import React from 'react';
import { CourseInterface } from "../interfaces/CourseInterface";
import './Course.css';

interface CourseProps {
	course: CourseInterface,
}

export class Course extends React.Component<CourseProps> {

	state = {
		divStyle: {}
	};

	constructor(props: CourseProps) {
		super(props);
		this.state = {
			divStyle: {
				zIndex: this.props.course.id,
				display: 'block'
			}
		};
	}
	componentDidMount(): void {
		console.log('...', this.props.course.name)
	}

	getStart = (e: React.MouseEvent) => {
		console.log('mouse down ', e.clientX);
	};

	getEnd = (e: React.MouseEvent) => {
		console.log('mouse down ', e.clientX);
		this.setState({
			divStyle: {
				zIndex: this.props.course.id,
				display: 'none'
			}
		});
	};

	render() {
		return (
			<div className="card course-card" style={this.state.divStyle} draggable={true} onDragStart={this.getStart} onDragEnd={this.getEnd}>
				{this.props.course.name}
			</div>
		)
	}

}
