import React from "react";
import { Skills } from "../data/Skills";
import { SkillInterface } from "../interfaces/SkillInterface";
import { SkillTag } from "./SkillTag";
import { AddedSkillInterface } from "../interfaces/AddedSkillInterface";
import { UserStateInterface } from "../store/user/UserStateInterface";
import { SystemStateInterface } from "../store/system/SystemStateInterface";
import { setUsername, setUserProfession, deselectSkillTag, selectSkillTag } from "../store/user/actions";
import { AppState } from "../store";
import { connect } from "react-redux";
import { thunkGetUserProfile } from "../thunks/thunkGetUserProfile";
import { SimpleInput } from "./SimpleInput";
import { Link } from "react-router-dom";

interface ProfileProps {
	user: UserStateInterface;
	system: SystemStateInterface;
	setUsername: typeof setUsername;
	setUserProfession: typeof setUserProfession;
	selectSkillTag: typeof selectSkillTag;
	deselectSkillTag: typeof deselectSkillTag;
	thunkGetUserProfile: any;
}

class ProfileComponent extends React.Component<ProfileProps> {

	componentDidMount(): void {
		// if i wanted to retrieve values via an asynchronous call
		this.props.thunkGetUserProfile();
	};

	getSkillDetails = (skillTag: AddedSkillInterface): SkillInterface|null => {
		return Skills.filter((_skillTag: SkillInterface) => _skillTag.name === skillTag.name).length > 0
			?  Skills.filter((_skillTag: SkillInterface) => _skillTag.name === skillTag.name)[0] : null;
	};

	updateUsername = (newUsername: string): void => {
		this.props.setUsername(newUsername);
	};

	updateUserprofession = (newUserprofession: string): void => {
		this.props.setUserProfession(newUserprofession);
	};

	render() {
		return (
			<div className="page">
				<div className="row">
					<div className="col-sm-12">
						<h3>
							<SimpleInput
								labelText={'Ich heiße'}
								inputValue={this.props.user.userName}
								updateValue={this.updateUsername}
							/>
						</h3>
						<h3>
							<SimpleInput
								labelText={'und bin'}
								inputValue={this.props.user.userProfession}
								updateValue={this.updateUserprofession}
							/>.
						</h3>
					</div>
					<div className="col-sm-12">
						<h3>
							Das sind meine&nbsp;
							<Link to={'/manage-skills'} className="link_color-white">
								Skills <span className="icon-edit"></span>
							</Link>:
						</h3>

						<div>
							{this.props.user.selectedSkillTags.map((skillTag: AddedSkillInterface, counter: number) => (
								<SkillTag
									key={counter}
									skillTag={this.getSkillDetails(skillTag)}
									selectSkill={this.props.selectSkillTag}
									deSelectSkill={this.props.deselectSkillTag}
									isSelectedSkill={true}
									isEditable={false}
								/>
							))}
							<Link to={'/manage-skills'} className="link_color-white">
								<span className="icon-edit"></span> Skills bearbeiten
							</Link>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state: AppState) => ({
	system: state.system,
	user: state.user
});

const Profile = connect(
	mapStateToProps, { setUsername, setUserProfession, selectSkillTag, deselectSkillTag, thunkGetUserProfile }
)(ProfileComponent);

export default Profile;
