import React from 'react';
import { SkillInterface } from "../interfaces/SkillInterface";
import { AddedSkillInterface } from "../interfaces/AddedSkillInterface";
import { SkillLevels } from "../enums/SkillLevels";

interface SkillTagProps {
	skillTag: SkillInterface | null,
	selectSkill: (event: AddedSkillInterface) => void;
	deSelectSkill: (event: AddedSkillInterface) => void;
	isSelectedSkill: boolean;
	isEditable: boolean;
}

export class SkillTag extends React.Component<SkillTagProps> {

	selectSkill = () => {
		if (!this.props.skillTag || !this.props.isEditable) {
			return;
		}
		let skillTag: AddedSkillInterface = {
			name: this.props.skillTag.name,
			added: new Date(),
			skillLevel: SkillLevels.Beginner
		};
		this.props.isSelectedSkill ? this.props.deSelectSkill(skillTag) : this.props.selectSkill(skillTag);
	};


	render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
		return (
			this.props.skillTag ?
				<button
					className={this.props.isSelectedSkill ? 'primary' : ''}
					onClick={this.selectSkill}>
					{this.props.skillTag.name}
				</button>
				: <span></span>
		);
	}
}
