import React from "react";
import {} from "react";
import { AppState } from "../store";
import { connect } from "react-redux";
import { acceptCourse, rejectCourse } from "../store/user/actions";
import { Courses } from "../data/Courses";
import { CourseInterface } from "../interfaces/CourseInterface";
import { Course } from "./Course";

class RecommendationsComponent extends React.Component<{}> {

	render() {
		return (
			<div className="page">
				<div className="row">
					<h3>Diese Weiterbildungsmöglichkeiten könnten für dich Sinn machen:</h3>
				</div>
				<div className="row">
					{(Courses.map((course: CourseInterface) => (
						<div className="col-sm-12">
							<Course
								key={course.id}
								course={course}
							/>
						</div>
					)))}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state: AppState) => ({
	system: state.system,
	user: state.user
});

const Recommendations = connect(
	mapStateToProps, {
		acceptCourse, rejectCourse
	}
)(RecommendationsComponent);

export default Recommendations;
