import React from "react";
import { NewsArray } from "../data/NewsArray"
import { NewsInterface } from "../interfaces/NewsInterface";

export default class News extends React.Component<{}> {
	render() {
		return (
			<div className="page">
				<div className="row">
					<h3>Das könnte dich interessieren...</h3>
				</div>
				<div className="row">
					{(NewsArray.map((news: NewsInterface, counter: number) => (
						<div className="col-sm-12">
							<div className="card fluid">
								<h4>{news.header}</h4>
								{ news.imageLink ? <img src={news.imageLink} alt={news.body}/> : ''}
								<p className="doc">{news.body}</p>
								{ news.link ? <p>Mehr : <a href={news.link} className="link_color-white" target="_blank" rel="noopener noreferrer">{news.link}</a></p> : ''}
							</div>
						</div>
					)))}
				</div>
			</div>
		);
	}
}

