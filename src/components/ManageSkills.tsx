import React from "react";
import { AppState } from "../store";
import { connect } from "react-redux";
import { deselectSkillTag, selectSkillTag } from "../store/user/actions";
import { SkillInterface } from "../interfaces/SkillInterface";
import { Skills } from "../data/Skills";
import { SkillTag } from "./SkillTag";
import { AddedSkillInterface } from "../interfaces/AddedSkillInterface";
import { UserStateInterface } from "../store/user/UserStateInterface";
import { SystemStateInterface } from "../store/system/SystemStateInterface";
import { Link } from "react-router-dom";

interface ManageSkillsProps {
	user: UserStateInterface;
	system: SystemStateInterface;
	selectSkillTag: typeof selectSkillTag;
	deselectSkillTag: typeof deselectSkillTag;
	thunkGetUserProfile: any;
}

class ManageSkillsComponent extends React.Component<ManageSkillsProps> {

	state = {
		lastSearch: '',
		currentResultSet: Skills
	};

	isSelectedSkill = (skillTag: SkillInterface): boolean => {
		return this.props.user.selectedSkillTags.filter((_skillTag: AddedSkillInterface) => _skillTag.name === skillTag.name).length > 0 ? true : false;
	};

	searchWithinResults = (e: React.ChangeEvent<HTMLInputElement>) => {
		console.log('e.target.value : ', e.target.value , ' vs. ', this.state.lastSearch);
		if (e.target.value.length === 0 || e.target.value !== this.state.lastSearch) {
			this.setState({
				lastSearch: e.target.value,
				currentResultSet: Skills
			});
		}
		if (e.target.value.length > 0) {
			console.log('yo2 !');
			this.setState({
				lastSearch: e.target.value,
				currentResultSet: Skills.filter(result => result.name.toString().toLowerCase().indexOf(e.target.value.toString().toLowerCase()) > -1)
			});
		}
	};



	render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
		let lastCategory: string|null = '';
		let returnSubtype = (subtype: string|null) => {
			lastCategory = subtype;
			if  (subtype) {
				return <div>
					<h4>{subtype}</h4>
					<div>
						{this.state.currentResultSet.filter((skillTag: SkillInterface) => skillTag.subtype ===  subtype)
							.map((skillTag: SkillInterface, counter: number) =>
								<SkillTag
									key={counter}
									skillTag={skillTag}
									selectSkill={this.props.selectSkillTag}
									deSelectSkill={this.props.deselectSkillTag}
									isSelectedSkill={this.isSelectedSkill(skillTag)}
									isEditable={true}
								/>
							)
						}
					</div>
				</div>
			} else {
				return;
			}
		};
		return (
			<div className="page">
				<div className="row">
					<div className="col-sm-12">
						<Link to={'/profile'} className="link_color-white">
							&lt; Zurück
						</Link>
					</div>
					<div className="col-sm-12">
						<h3>Suche nach Skills</h3>
						<h3>
							<input
								id="typeahead"
								placeholder="Suche nach einem Skill"
								onChange={this.searchWithinResults}/>
						</h3>
						<h3>und/oder selektiere Skills</h3>
						{this.state.currentResultSet.map((skillTag: SkillInterface, counter: number) =>
							(lastCategory !== skillTag.subtype ? returnSubtype(skillTag.subtype) : '')
						)}
					</div>
				</div>
			</div>
		)
	}

}
/*
<SkillTag
	key={counter}
	skillTag={skillTag}
	selectSkill={this.props.selectSkillTag}
	deSelectSkill={this.props.deselectSkillTag}
	isSelectedSkill={this.isSelectedSkill(skillTag)}
	isEditable={true}
/>
*/

const mapStateToProps = (state: AppState) => ({
	system: state.system,
	user: state.user
});

const ManageSkills = connect(
	mapStateToProps, { selectSkillTag, deselectSkillTag }
)(ManageSkillsComponent);

export default ManageSkills;