import React from 'react';

interface SimpleInputProps {
	labelText: string,
	inputValue: string,
	updateValue: (event: string) => void;
}

export class SimpleInput extends React.Component<SimpleInputProps> {

	state = {
		value: this.props.inputValue,
		isEditing: false
	};

	handleChangeEvent = (e:  React.ChangeEvent<HTMLInputElement>): void => {
		this.setState({value: e.target.value});
	};

	handleKeyPressedEvent = (e: React.KeyboardEvent): void => {
		if (e.keyCode === 13) {
			this.props.updateValue(this.state.value);
			this.setState({isEditing: false});
		}
	};

	setToEditMode = (e: React.MouseEvent): void => {
		this.setState({isEditing: true});
	};

	render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
		return (
			<span>
				<span>{this.props.labelText}&nbsp;</span>
				{ (this.props.inputValue.length > 0 && !this.state.isEditing) ? (
					<span className="text-link" onClick={this.setToEditMode}>{this.props.inputValue}</span>
				) : (
					<input type="text"
						id="username"
						placeholder={this.props.inputValue}
						onChange={this.handleChangeEvent}
						onKeyDown={this.handleKeyPressedEvent}
						value={this.state.value}
					/>
				)}
			</span>
		);
	}
}
