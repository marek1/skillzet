import { SystemStateInterface } from "./SystemStateInterface";
import { SET_LANGUAGE, SystemActionTypes } from "./types";

const initialState: SystemStateInterface = {
	language: 'DE'
};

export function systemReducer(
	state = initialState,
	action: SystemActionTypes
): SystemStateInterface {
	switch (action.type) {
		case SET_LANGUAGE:
			return {
				language: action.payload
			};
		default:
			return state;
	}
}