import { SkillInterface } from "../../interfaces/SkillInterface";
import { SET_LANGUAGE } from "./types";

export function setUsername(language: string) {
	return {
		type: SET_LANGUAGE,
		payload: language
	}
}
