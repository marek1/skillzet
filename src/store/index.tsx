import { applyMiddleware, combineReducers, createStore } from "redux";
import { systemReducer } from "./system/reducers";
import { userReducer } from "./user/reducers";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

const rootReducer = combineReducers({
	system: systemReducer,
	user: userReducer
});

export type AppState = ReturnType<typeof rootReducer>;


export default function configureStore() {
	const middlewares = [thunkMiddleware];
	const middleWareEnhancer = applyMiddleware(...middlewares);

	const store = createStore(
		rootReducer,
		composeWithDevTools(middleWareEnhancer)
	);

	return store;
}
