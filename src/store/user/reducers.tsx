import { UserStateInterface } from "./UserStateInterface";
import {
	ACCEPT_COURSE,
	DESELECT_SKILLTAG,
	REJECT_COURSE,
	SELECT_SKILLTAG,
	SET_USERNAME, SET_USERPROFESSION,
	UserActionTypes
} from "./types";

const initialState: UserStateInterface = {
	userName: '',
	userProfession: '',
	selectedSkillTags: [],
	acceptedCourses: [],
	rejectedCourses: []
};

const setStateInLocalStorage = (newState: any) => {
	window.localStorage.setItem('skillzet_UserProfile', JSON.stringify(newState));
	return newState;
};
export function userReducer(
	state = initialState,
	action: UserActionTypes
): UserStateInterface {
	console.log('action.type : ' + action.type);
	console.log('action.payload : ' + action.payload);
	switch (action.type) {
		case SET_USERNAME:
			return setStateInLocalStorage({
				...state,
				userName: action.payload
			});
		case SET_USERPROFESSION:
			return setStateInLocalStorage({
				...state,
				userProfession: action.payload
			});
		case SELECT_SKILLTAG:
			// do not store already stored skills
			if (state.selectedSkillTags.filter(selectedSkillTag => selectedSkillTag.name === action.payload.name).length> 0 ) {
				return state;
			}
			return setStateInLocalStorage({
				...state,
				selectedSkillTags: [...state.selectedSkillTags, action.payload]
			});
		case DESELECT_SKILLTAG:
			return setStateInLocalStorage({
				...state,
				selectedSkillTags: state.selectedSkillTags.filter(
					selectedSkillTag => selectedSkillTag.name !== action.payload.name
				)
			});
		case ACCEPT_COURSE:
			return setStateInLocalStorage({
				...state,
				acceptedCourses: [...state.acceptedCourses, action.payload]
			});
		case REJECT_COURSE:
			return setStateInLocalStorage({
				...state,
				rejectedCourses: [...state.rejectedCourses, action.payload]
			});
		default:
			return state;
	}
}