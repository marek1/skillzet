import { AddedSkillInterface } from "../../interfaces/AddedSkillInterface";
import { CourseInterface } from "../../interfaces/CourseInterface";

export interface UserStateInterface {
    userName: string;
    userProfession: string;
    selectedSkillTags: AddedSkillInterface[];
    acceptedCourses: CourseInterface[];
    rejectedCourses: CourseInterface[];
}
