import {
	ACCEPT_COURSE,
	DESELECT_SKILLTAG,
	REJECT_COURSE,
	SELECT_SKILLTAG,
	SET_USERNAME,
	SET_USERPROFESSION
} from "./types";
import { AddedSkillInterface } from "../../interfaces/AddedSkillInterface";
import { CourseInterface } from "../../interfaces/CourseInterface";

export function setUsername(userName: string) {
	return {
		type: SET_USERNAME,
		payload: userName

	}
}

export function setUserProfession(userProfession: string) {
	return {
		type: SET_USERPROFESSION,
		payload: userProfession
	}
}

export function selectSkillTag(newSkillTag: AddedSkillInterface) {
	return {
		type: SELECT_SKILLTAG,
		payload: newSkillTag
	}
}

export function deselectSkillTag(oldSkillTag: AddedSkillInterface) {
	return {
		type: DESELECT_SKILLTAG,
		payload: oldSkillTag
	}
}

export function acceptCourse(course: CourseInterface) {
	return {
		type: ACCEPT_COURSE,
		payload: course
	}
}

export function rejectCourse(course: CourseInterface) {
	return {
		type: REJECT_COURSE,
		payload: course
	}
}
