import { AddedSkillInterface } from "../../interfaces/AddedSkillInterface";
import { CourseInterface } from "../../interfaces/CourseInterface";

export const SET_USERNAME = 'SET_USERNAME';
export const SET_USERPROFESSION = 'SET_USERPROFESSION';
export const SELECT_SKILLTAG = 'SELECT_SKILLTAG';
export const DESELECT_SKILLTAG = 'DESELECT_SKILLTAG';
export const ACCEPT_COURSE = 'ACCEPT_COURSE';
export const REJECT_COURSE = 'ACCEPT_COURSE';

interface SetUserNameAction {
	type: typeof SET_USERNAME;
	payload: string;
}

interface SetUserProfessionAction {
	type: typeof SET_USERPROFESSION;
	payload: string;
}

interface SelectSkillTagAction {
	type: typeof SELECT_SKILLTAG;
	payload: AddedSkillInterface;
}

interface DeselectSkillTagAction {
	type: typeof DESELECT_SKILLTAG;
	payload: AddedSkillInterface;
}

interface AcceptCourseAction {
	type: typeof ACCEPT_COURSE;
	payload: CourseInterface;
}

interface RejectCourseAction {
	type: typeof REJECT_COURSE;
	payload: CourseInterface;
}

export type UserActionTypes =
	SetUserNameAction |
	SetUserProfessionAction |
	SelectSkillTagAction |
	DeselectSkillTagAction |
	AcceptCourseAction |
	RejectCourseAction;
